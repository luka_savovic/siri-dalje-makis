import { createApp } from 'vue'
import App from './App.vue'
// import VueSocialSharing from 'vue-social-sharing'
 
const myApp = createApp(App);
// myApp.use(VueSocialSharing);
myApp.mount('#app')
